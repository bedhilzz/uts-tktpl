package id.ac.ui.cs.mobileprogramming.muhammadfadhillah.speedtest;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.ac.ui.cs.mobileprogramming.muhammadfadhillah.speedtest.util.DownloadFileAsync;

public class DownloadFragment extends Fragment implements OnDownloadListener {
    private static final String ARG_SECTION_NUMBER = "section_number";

    @BindView(R.id.bt_download_10)
    Button btDownload10MB;

    @BindView(R.id.bt_download_25)
    Button btDownload25MB;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    @BindView(R.id.tv_download_rate)
    TextView tvDownloadRate;

    private DownloadFileAsync downloadFileAsync;

    public DownloadFragment() {
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (downloadFileAsync != null) {
            downloadFileAsync.cancel(true);
        }
    }

    public static DownloadFragment newInstance(int sectionNumber) {
        DownloadFragment fragment = new DownloadFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_download, container, false);
        ButterKnife.bind(this, rootView);

        initComponent();

        return rootView;
    }

    @Override
    public void onDownloadProgress(int progress, int loaded, int total) {
        progressBar.setProgress(progress);

        String loadedStr = String.format(Locale.US, "%,d", loaded);
        String totalStr = String.format(Locale.US, "%,d", total);
        String downloadRate = String.format(getString(R.string.download_rate), loadedStr, totalStr);
        tvDownloadRate.setText(downloadRate);
    }

    private void initComponent() {
        initDownloadRate();
        initDownloadButton();
    }

    private void initDownloadRate() {
        int dummyValue = 0;
        String dummyStr = String.format(Locale.US, "%d", dummyValue);
        String dummyDownloadRate = String.format(getString(R.string.download_rate), dummyStr, dummyStr);
        tvDownloadRate.setText(dummyDownloadRate);
    }

    private void initDownloadButton() {
        btDownload10MB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                download(getString(R.string.url_file_download_10mb));
            }
        });

        btDownload25MB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                download(getString(R.string.url_file_download_25mb));
            }
        });
    }

    private void download(String fileURL) {
        final DownloadFragment context = this;

        if (downloadFileAsync != null) {
            downloadFileAsync.cancel(true);
        }

        downloadFileAsync = new DownloadFileAsync(context, fileURL);

        downloadFileAsync.execute();
    }
}
