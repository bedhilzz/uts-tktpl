package id.ac.ui.cs.mobileprogramming.muhammadfadhillah.speedtest;

public interface OnDownloadListener {
    void onDownloadProgress(int progress, int loaded, int total);
}
