package id.ac.ui.cs.mobileprogramming.muhammadfadhillah.speedtest.util;

public class Constants {
    public static final String FILE_LINK_10MB = "http://www.ovh.net/files/10Mio.dat";
    public static final String FILE_LINK_25MB = "http://www.ovh.net/files/100Mio.dat";
}
