package id.ac.ui.cs.mobileprogramming.muhammadfadhillah.speedtest.util;

import android.os.AsyncTask;
import android.util.Log;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import id.ac.ui.cs.mobileprogramming.muhammadfadhillah.speedtest.OnDownloadListener;

public class DownloadFileAsync extends AsyncTask<String, Integer, String> {
    private final String LOG_TAG = this.getClass().getSimpleName();
    private OnDownloadListener mContext;
    private String fileURL;

    public DownloadFileAsync(OnDownloadListener mContext, String fileURL) {
        this.mContext = mContext;
        this.fileURL = fileURL;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            //connecting to url
            URL u = new URL(fileURL);
            HttpURLConnection c = (HttpURLConnection) u.openConnection();
            c.setRequestMethod("GET");
            c.setDoOutput(true);
            c.connect();

            // contentLength is used for calculating download progress
            int contentLength = c.getContentLength();

            //file input is from the url
            InputStream in = c.getInputStream();

            //here’s the download code
            byte[] buffer = new byte[1024];
            int contentRead = 0;
            long contentLoaded = 0;

            while ((contentRead = in.read(buffer)) > 0) {
                contentLoaded += contentRead;

                publishProgress((int) ((contentLoaded * 100)/contentLength),
                        (int) contentLoaded,
                        contentLength
                );
            }

        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);

        int byteFactor = 1024;
        int progress = values[0];
        int loaded = values[1]/byteFactor;
        int total = values[2]/byteFactor;

        try {
            mContext.onDownloadProgress(progress, loaded, total);
        } catch (Exception e) {
            Log.d(LOG_TAG, e.getMessage());
        }
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
    }
}
